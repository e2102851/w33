import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import birds from './bird.json';

//console.log(birds)
const TableExample =()=>{   

      const HeaderStyle = {
          backgroundColor: 'lightgray',
          border: '1px solid gray',
        
      }

      const bold = {
        fontWeight: 'bold', 
        textAlign: 'left'
      }

      const textAlignLeft = {
        textAlign: 'left',
        border: '1px solid lightblue',
      }

      const sortedBirds = birds.sort(function(a,b){
        return a.finnish.localeCompare(b.finnish);
        }) 

      return(
        <div>
            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                    <TableHead>
                    <TableRow style={HeaderStyle}>
                        <TableCell style={bold}>Finnish</TableCell>
                        <TableCell style={bold}>English</TableCell>
                        <TableCell style={bold}>Swedish</TableCell>
                        <TableCell style={bold}>Short</TableCell>
                        <TableCell style={bold}>Latin</TableCell>
                    </TableRow>
                    </TableHead>
                    <TableBody>
                    
                   { console.log(birds) }    
                    {sortedBirds.map((bird) => (
                      
                    

                        <TableRow
                        key={bird.finnish}
                        sx={{ '&:last-child td, &:last-child th': { border: 1 } }}
                        >
                        <TableCell component="th" scope="row">
                            {bird.finnish}
                        </TableCell>
                        <TableCell style={textAlignLeft}>{bird.english}</TableCell>
                        <TableCell style={textAlignLeft}>{bird.swedish}</TableCell>
                        <TableCell style={textAlignLeft}>{bird.short}</TableCell>
                        <TableCell style={textAlignLeft}>{bird.latin}</TableCell>
                        </TableRow>
                    ))}
                    </TableBody>
                </Table>
                </TableContainer>
        </div>
    )
}

export default TableExample;